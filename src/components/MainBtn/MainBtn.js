export default {
  props: {
    button_text: {
      type: String,
      required: true,
    }, 
    button_url: {
      type: String,
      required: true,
    }
  } 
};